package connection

import (
  "testing"
  "bitbucket.org/zh-ang/api.11buhe.com/confreader"
)

var myconf = confreader.ReadMysqlConf()

var (
  myDiverName = myconf.Mysql.DriverName
  myDestSourceName = myconf.Mysql.DestSourceName
)

func TestMySQLConn(t *testing.T) {
  DBP := NewConn(myDiverName, myDestSourceName)

  err := DBP.OpenConnForMySQL()
  if err != nil {
    t.Error(err)
  }

  err = DBP.Conn.Ping()
  if err != nil {
    t.Error(err)
  }

  DBP.Conn.Exec("")

}
