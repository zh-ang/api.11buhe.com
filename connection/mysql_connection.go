package connection

import (
  "errors"
  "database/sql"
  _ "github.com/go-sql-driver/mysql"
)

func NewConn(diverName, destSourceName string) *DBConnPersist {
  return &DBConnPersist{
    diverName: diverName,
    destSourceName: destSourceName,
  }
}

type DBConnPersist struct {
  diverName string
  destSourceName string
  Conn *sql.DB
}

func (DCP *DBConnPersist) OpenConnForMySQL() error {
  db, err := sql.Open(DCP.diverName, DCP.destSourceName)
  if err != nil {
    return err
  }
  DCP.Conn = db
  return nil
}

func (DCP *DBConnPersist) PingForMySQL() error {
  if DCP.Conn == nil {
    return errors.New("Pinging database must be after database opened")
  }
  return DCP.Conn.Ping()
}

func (DCP *DBConnPersist) CloseMySQL() error {
  if DCP.Conn == nil {
    return errors.New("No connection will be closed")
  }
  DCP.Conn.Close()
  return nil
}
