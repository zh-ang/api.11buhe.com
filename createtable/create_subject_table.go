package createtable

import (
  "bitbucket.org/zh-ang/api.11buhe.com/connection"
  "bitbucket.org/zh-ang/api.11buhe.com/confreader"
)

var (
  createTableSubjectSQL = `
    CREATE TABLE buhe_subject (
	    id BIGINT NOT NULL AUTO_INCREMENT,
	    user_id BIGINT NOT NULL,
	    CONSTRAINT FK_ID FOREIGN KEY (user_id) REFERENCES buhe_user (id),
	    content TEXT NOT NULL,
	    good MEDIUMINT,
      lower MEDIUMINT,
      favortie MEDIUMINT,
      relay MEDIUMINT,
	    PRIMARY KEY (id),
	    INDEX i_uid (user_id),
	    FULLTEXT i_text (content)
    ) AUTO_INCREMENT;
  `
)

func CetTableSubject() error {
  myconf := confreader.ReadMysqlConf()

  DBP := connection.NewConn(myconf.Mysql.DriverName, myconf.Mysql.DestSourceName)
  err := DBP.OpenConnForMySQL()
  if err != nil {
    return err
  }

  err = DBP.Conn.Ping()
  if err != nil {
    return err
  }

  if _, err = DBP.Conn.Exec(createTableSubjectSQL); err != nil {
    panic(err)
  }

  return nil
}
