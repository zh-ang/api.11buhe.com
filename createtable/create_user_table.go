package createtable

import (
  "bitbucket.org/zh-ang/api.11buhe.com/connection"
  "bitbucket.org/zh-ang/api.11buhe.com/confreader"
)

var (
  createTableUserSQL = `
    CREATE TABLE buhe_user (
      id BIGINT NOT NULL AUTO_INCREMENT,
      username VARCHAR(50) NOT NUll,
      email VARCHAR(100) NOT NUll DEFAULT '',
      mobile VARCHAR(11) NOT NULL DEFAULT '',
      avatar VARCHAR(255) NOT NULL DEFAULT '',
      password VARCHAR(100) NOT NULL,
      PRIMARY KEY(id)
    ) AUTO_INCREMENT=10000;
  `
)

func CetTableUser() error {
  myconf := confreader.ReadMysqlConf()

  DBP := connection.NewConn(myconf.Mysql.DriverName, myconf.Mysql.DestSourceName)
  err := DBP.OpenConnForMySQL()
  if err != nil {
    return err
  }

  err = DBP.Conn.Ping()
  if err != nil {
    return err
  }

  if _, err = DBP.Conn.Exec(createTableUserSQL); err != nil {
    panic(err)
  }

  return nil
}
