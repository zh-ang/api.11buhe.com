package confreader

import (
  "encoding/json"
  "io/ioutil"
  "runtime"
  "path"
)

func GetMySQLConfFilePath() string  {
  _, filename, _, ok := runtime.Caller(1)
  if !ok {
    panic("ERROR 1024: Get current directory failed")
  }
  return path.Join(path.Dir(filename), "./table.json")
}


type MySQLTableUser struct {
  Mysql mysqlConf `json:"mysql"`
}

type mysqlConf struct {
  DriverName      string `json:"driverName"`
  DestSourceName string `json:"destSourceName"`
}

func ReadMysqlConf() (myconf MySQLTableUser) {
  contents, err := ioutil.ReadFile(GetMySQLConfFilePath())
  if err != nil {
    panic("ERRPR 1025: Configuration file for connecting mysql that not found")
  }

  err = json.Unmarshal(contents, &myconf)
  if err != nil {
    panic(err)
  }

  return myconf
}
