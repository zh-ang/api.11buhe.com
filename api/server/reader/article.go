package reader

import (
  "github.com/gin-gonic/gin"
  "net/http"
)

func SingleArticleReader(c *gin.Context) {
  name := c.Param("id")
  c.String(http.StatusOK, "Hello %s", name)
}
