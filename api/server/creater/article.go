package creater

import (
  "github.com/gin-gonic/gin"
  "net/http"
  "fmt"
)

func ComposeArticle(c *gin.Context) {
  cookie := c.Request.Header.Get("Cookie")
  fmt.Println(cookie)
  content := c.PostForm("content")
  c.JSON(http.StatusOK, content)
}
