package router

import (
  "github.com/gin-gonic/gin"
  "net/http"
  "bitbucket.org/zh-ang/api.11buhe.com/api/server/creater"
  "bitbucket.org/zh-ang/api.11buhe.com/api/server/reader"
)

func indexHome(c *gin.Context) {
  c.JSON(http.StatusOK, gin.H{
    "current_url": "http://api.11buhe.com/",
    "version": "0.1.0",
    "client_address": c.Request.RemoteAddr,
  })
}

func RegisterRouter() *gin.Engine {
  R := gin.Default()

  // 主页
  R.GET("/", indexHome)

  // 查看单一段子
  R.GET("article/:id", reader.SingleArticleReader)

  // 提交段子
  R.POST("compose/article", creater.ComposeArticle)
  return R
}
