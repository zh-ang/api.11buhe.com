package db

import (
  "bitbucket.org/zh-ang/api.11buhe.com/connection"
  "bitbucket.org/zh-ang/api.11buhe.com/confreader"
)

// 主题
type SubjectSchema struct {
  Userid int64
  Content string
  Good int32
  Lower int32
  Favorite int32
  Relay int32
}


// 插入主题
func InsertSubject(sub *SubjectSchema) error {
  myconf := confreader.ReadMysqlConf()
  DBP := connection.NewConn(myconf.Mysql.DriverName, myconf.Mysql.DestSourceName)
  err := DBP.OpenConnForMySQL()
  if err != nil {
    return err
  }

  err = DBP.Conn.Ping()
  if err != nil {
    return err
  }

  cpsSQL := "INSERT INTO buhe_subject (user_id, content, good, lower, favorite, relay) VALUES(?, ?, ?, ?, ?, ?)"
  stmtIns, err := DBP.Conn.Prepare(cpsSQL)
  if err != nil {
    return err
  }

  _, err = stmtIns.Exec(sub.Userid, sub.Content, sub.Good, sub.Lower, sub.Favorite, sub.Relay)
  if err != nil {
    return err
  }

  return nil
}
