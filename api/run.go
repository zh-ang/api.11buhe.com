package main

import (
  "bitbucket.org/zh-ang/api.11buhe.com/api/server/router"
  "log"
)

func main() {
  r := router.RegisterRouter()
  log.Fatal(r.Run(":4343"))
}
